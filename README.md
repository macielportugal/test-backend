# Akna Software - Analista Programador PHP

## Teste usando docker

```bash
docker-compose build
docker-compose up -d
docker-compose exec db mysql -uroot -p123456 test-backend -e "source /schema.sql"
docker-compose exec web php test.php csv // Arg: csv, mysql ou all 
```

## Teste sem docker

```bash
mysql -uroot -p database < docker/mysql/schema.sql
php test.php csv // Arg: csv, mysql ou all 
```