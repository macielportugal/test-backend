create table purchase_list (

    id int(11) NOT NULL AUTO_INCREMENT,
    `month` char(100) NOT NULL,
    category char(200) NOT NULL,
    product char(100) NOT NULL,
    amount FLOAT NOT NULL,
    updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    created_at TIMESTAMP NOT NULL,
    primary key (`id`)

);