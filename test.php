<?php
require 'PurchaseList.php';

$purchaseList = new PurchaseList(include('lista-de-compras.php'));

$test = (count($argv) > 1) ? $argv[1] : 'csv'; 

if ($test == 'mysql' || $test == 'all') {
    echo "\nGerando Mysql\n";

    $purchaseList->save();
}

if ($test !== 'mysql') {
    echo "\nGerando CSV\n";

    $purchaseList->generateCSV('compras-do-ano.csv');
}
