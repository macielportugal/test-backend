<?php

class PurchaseList
{
    private $items;

    public function __construct(array $items) {
        $this->items = $items;
        $this->ordination();
    }

    public function ordination() {
        uksort($this->items, function ($a, $b) {
            $months = [
                'janeiro',
                'fevereiro',
                'marco',
                'março',
                'abril',
                'maio',
                'junho',
                'julho',
                'agosto',
                'setembro',
                'outubro',
                'novembro',
                'dezembro'
            ];

            $key1 = array_search(strtolower($a), $months);
            $key2 = array_search(strtolower($b), $months);

            if ($key1 == $key2) {
                return 0;
            }
            
            return ($key1 > $key2) ? 1 : -1;
        });

        foreach ($this->items as &$item) {
            ksort($item, SORT_NATURAL);

            foreach ($item as &$product) {
                uasort($product, function($a, $b) {
                    if ($a == $b) {
                        return 0;
                    }

                    return ($a < $b) ? 1 : -1;
                });
            }
        }
    }

    private function adjustment($text) {
        $text = strtolower(str_replace('_', ' ', $text));

        $correctWords = [
            'marco' => 'março',
            'hignico' => 'higiênico',
            'brocolis' => 'brócolis',
            'sabao em po' => 'sabão em pó',
            'sabao em po' => 'sabão em pó',
            'chocolate ao leit' => 'Chocolate ao leit'
        ];

        $text = str_replace(array_keys($correctWords), $correctWords, $text);
        $text = ucfirst($text);

        return $text;
    }

    private function walkList($callback) {
        foreach ($this->items as $month => $categories) {
            foreach ($categories as $category => $products) {
                foreach ($products as $product => $amount) {
                    $callback($this->adjustment($month), $this->adjustment($category), $this->adjustment($product), $amount);
                }
            }
        }
    }

    public function generateCSV(string $filename) {
        $filecsv = fopen($filename, 'w+');    
        fputcsv($filecsv, array('Mês', 'Categoria', 'Produto', 'Quantidade'));

        $this->walkList(function($month, $category, $product, $amount) use ($filecsv) {
            fputcsv($filecsv, array($month, $category, $product, $amount));
        });       
    }

    public function save() {
        $db = new PDO('mysql:host=db;dbname=test-backend', 'root', '123456');

        $this->walkList(function($month, $category, $product, $amount) use ($db) {
            $stmt = $db->prepare("INSERT INTO purchase_list(`month`, `category`, `product`, `amount`) VALUES (?, ?, ?, ?)");
            $stmt->bindParam(1, $month);
            $stmt->bindParam(2, $category);
            $stmt->bindParam(3, $product);
            $stmt->bindParam(4, $amount);
            $stmt->execute();
        });
    }

}